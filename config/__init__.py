# --coding: utf-8 --
import os
import platform

# APP MAIN DIRS
APP_DIR = os.path.join(os.path.dirname(__file__), '..')
CONFIG_DIR = os.path.join(APP_DIR, 'config')
LOGS_DIR = os.path.join(APP_DIR, "logs")
TEMPLATES_DIR = os.path.join(os.path.dirname(__file__), '..', 'Crimestoppers', 'templates/')


# CRIME CONSTANTS
CRIME_BASE_URL = 'https://crimestoppers-uk.org'
CRIME_MOST_WANTED_URL = 'https://crimestoppers-uk.org/give-information/most-wanted'
IMPLICITLY_WAIT_TIME = 300

# OS settings
if platform.system() == "Linux":
    WEB_DRIVER_PATH = os.path.abspath(os.path.join(os.getcwd(), 'utils/web_driver/linux/geckodriver'))
else:
    WEB_DRIVER_PATH = os.path.abspath(os.path.join(os.getcwd(), 'utils/web_driver/win/geckodriver'))

