# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import config


def get_raw_html(start_html_element, end_html_element):
    full_details = ''
    while True:
            full_details += str(start_html_element)
            start_html_element = start_html_element.next

            if start_html_element == end_html_element:
                break
    return full_details


def clean_text_from_tags(tags, text):
    for x, y in tags.items():
        text = text.replace(x, y)
    return text


def create_firefox_browser(headless):
    options = Options()
    options.set_headless(headless=headless)
    fp = webdriver.FirefoxProfile()
    browser = webdriver.Firefox(firefox_options=options, firefox_profile=fp, executable_path=config.WEB_DRIVER_PATH)
    return browser