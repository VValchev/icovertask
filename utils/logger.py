import config
import logging
from logzero import setup_logger


class Logger:

    def __init__(self):
        self.info_logger = setup_logger(
            name="info_logger",
            logfile=config.LOGS_DIR + '/info.log',
            level=logging.INFO
        )
        self.error_logger = setup_logger(
            name="error_logger",
            logfile=config.LOGS_DIR + '/error.log',
            level=logging.ERROR
        )