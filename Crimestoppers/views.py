# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.decorators import api_view
from django.http import JsonResponse
from utils.utils import create_firefox_browser
from .models import CrimeModel
from .serializers import CrimelistSerializer
from .scrapper import crime_scrap, CrimeScrapper, save_crime
import logging


def start_scraper(request):
    # functional style
    # crime_scrap()

    # class style
    browser = create_firefox_browser(headless=True)
    scraper = CrimeScrapper(browser=browser)
    result = scraper.scrape_data()
    if result:
        save_crime(scraper.crime_data)
    else:
        scraper.log_errors(logger=logging.getLogger('error_logger'))

    return render(request, 'index.html')


@api_view(["GET"])
def crime_list(request):

    queryset = CrimeModel.objects.all()
    serializer = CrimelistSerializer(queryset, many=True)
    return JsonResponse(serializer.data, safe=False)


@api_view(["GET"])
def crime_detail(request, pk):

    try:
        queryset = CrimeModel.objects.get(pk=pk)
    except CrimeModel.DoesNotExist:
        return HttpResponse(status=404)

    serializer = CrimelistSerializer(queryset)
    return JsonResponse(serializer.data)
