# -*- coding: utf-8 -*-
from rest_framework import serializers
from .models import CrimeModel


class CrimelistSerializer(serializers.ModelSerializer):

    class Meta:

        model = CrimeModel
        fields = ('id', 'crime_url', 'crime_pic_src', 'crime_type', 'crime_location', 'suspect_name', 'nickname',
                  'number_of_people_involved', 'cs_reference', 'police_force', 'summary', 'full_details', 'whereabouts',
                  'sex', 'age_range', 'height', 'build', 'hair_colour', 'hair_type', 'facial_hair', 'ethnic_appearance',
                  'additional_information')


