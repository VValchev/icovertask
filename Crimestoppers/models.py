# -*- coding: utf-8 -*-
from django.db import models


class CrimeModel(models.Model):

    crime_url = models.CharField(max_length=255, blank=True)
    crime_pic_src = models.CharField(max_length=255, blank=True)
    crime_type = models.CharField(max_length=255, blank=True)
    crime_location = models.CharField(max_length=255, blank=True)
    suspect_name = models.CharField(max_length=255, blank=True)
    nickname = models.CharField(max_length=255, blank=True)
    number_of_people_involved = models.CharField(max_length=255, blank=True)
    cs_reference = models.CharField(max_length=255, blank=True )
    police_force = models.CharField(max_length=255, blank=True)
    summary= models.TextField(blank=True)
    full_details = models.TextField(blank=True)
    whereabouts = models.CharField(max_length=100, blank=True)
    sex = models.CharField(max_length=30, blank=True)
    age_range = models.CharField(max_length=30, blank=True)
    height = models.CharField(max_length=100, blank=True)
    build = models.CharField(max_length=30, blank=True)
    hair_colour = models.CharField(max_length=30, blank=True)
    hair_type = models.CharField(max_length=30, blank=True)
    facial_hair = models.CharField(max_length=30, blank=True)
    ethnic_appearance = models.CharField(max_length=100, blank=True)
    additional_information = models.TextField(blank=True)

    def __str__(self):

        return self.suspect_name
