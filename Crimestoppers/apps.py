from django.apps import AppConfig
from utils.logger import Logger


class CrimestoppersConfig(AppConfig):
    name = 'Crimestoppers'

    def ready(self):
        Logger()
