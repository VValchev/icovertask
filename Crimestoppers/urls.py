# -*- coding: utf-8 -*-
from django.urls import path
from Crimestoppers import views


urlpatterns = [
    path('', views.start_scraper, name="index"),
    path('index/', views.start_scraper, name="index"),
    path('crime/', views.crime_list, name="crime_list"),
    path('crime/<int:pk>/', views.crime_detail, name="crime_detail")
]
