# -*- coding: utf-8 -*-
import config
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from bs4 import BeautifulSoup
from selenium.common.exceptions import NoSuchElementException
from utils.utils import get_raw_html, clean_text_from_tags
from .models import CrimeModel
import pprint
from random import randint
from abc import ABC, abstractmethod
import logging

options = Options()
options.set_headless(headless=False)
fp = webdriver.FirefoxProfile()


# start scrape most wanted crimes
def crime_scrap():
    print('Start scraping crimes.')
    browser = webdriver.Firefox(
        firefox_options=options, firefox_profile=fp, executable_path=config.WEB_DRIVER_PATH)

    # get urls for pages with crimes
    page_urls = get_pages_urls(browser)

    # get urls for all crimes
    crime_urls = get_crime_urls(browser, page_urls)

    # get crime data
    crime_data = get_crime_details(browser, crime_urls)
    browser.close()

    # save crime data
    save_crime(crime_data)
    print('Done scraping crimes.')


# get all pages with crimes
def get_pages_urls(browser):

    try:
        paginator_urls = []
        most_wanted_url = config.CRIME_MOST_WANTED_URL
        browser.maximize_window()
        browser.get(most_wanted_url)
        browser.implicitly_wait(config.IMPLICITLY_WAIT_TIME)
        page_source = browser.page_source
        html_soup = BeautifulSoup(page_source, 'html.parser')

        paginator_links = html_soup.find_all('a', class_='page-link')
        for link in paginator_links:
            paginator_urls.append(link.get('href'))

        paginator_urls = list(set(paginator_urls))

        return paginator_urls

    except Exception as ex:
        print('Error paginator urls can`t be extracted.')
        print(ex)
        browser.close()


# get all urls for crime information
def get_crime_urls(browser, page_urls):

    crimes_urls = []

    while page_urls:
        p_url = page_urls.pop()

        try:
            page_url = config.CRIME_BASE_URL + p_url
            browser.maximize_window()
            browser.get(page_url)
            browser.implicitly_wait(config.IMPLICITLY_WAIT_TIME)
            page_source = browser.page_source
            html_soup = BeautifulSoup(page_source, 'html.parser')

            figures = html_soup.find_all('figure', class_='d-flex align-items-center')

            for figure in figures:

                a = figure.find('a')
                crimes_urls.append(a.get('href'))

            # time.sleep(randint(0, 9))

        except Exception as ex:
            print('Crime url can`t be extracted.')
            print(ex)
            browser.close()

    return crimes_urls


# get all crimes information
def get_crime_details(browser, crime_urls):

    data = []

    while crime_urls:

        crime_data = {}
        url = crime_urls.pop()
        crime_url = config.CRIME_BASE_URL + url

        try:
            browser.maximize_window()
            browser.get(crime_url)
            browser.implicitly_wait(config.IMPLICITLY_WAIT_TIME)
            page_source = browser.page_source
            html_soup = BeautifulSoup(page_source, 'html.parser')
        except Exception as ex:
            print('Crime page can`t be loaded.')
            print(ex)
            browser.close()

        # Crime url
        crime_data['crime_url'] = crime_url
        print('crime_url -> ', crime_data['crime_url'])

        # Wanted intro section
        try:
            crime_data['crime_pic_src'] = html_soup.find('figure').find('img').get('src')
        except Exception as ex:
            crime_data['crime_pic_src'] = ''
        print('crime_pic_src -> ', crime_data['crime_pic_src'])

        try:
            crime_data['crime_type'] = html_soup.find('strong', string='Crime type:').parent.text\
                .replace('Crime type:', '').strip()
        except Exception as ex:
            crime_data['crime_type'] = ''
        print('crime_type -> ', crime_data['crime_type'])

        try:
            crime_data['crime_location'] = html_soup.find('strong', string='Crime location:').parent.text\
                .replace('Crime location:', '').strip()
        except Exception as ex:
            crime_data['crime_location'] = ''
        print('crime_location -> ', crime_data['crime_location'])

        try:
            crime_data['suspect_name'] = html_soup.find('strong', string='Suspect name:').parent.text\
                .replace('Suspect name:', '').strip()
        except Exception as ex:
            crime_data['suspect_name'] = ''
        print('suspect_name -> ', crime_data['suspect_name'])

        try:
            crime_data['nickname'] = html_soup.find('strong', string='Nickname:').parent.text\
                .replace('Nickname:', '').strip()
        except Exception as ex:
            crime_data['nickname'] = ''
        print('nickname -> ', crime_data['nickname'])

        try:
            crime_data['number_of_people_involved'] = html_soup.find('strong', string='Number of people involved:').parent.text \
                .replace('Number of people involved:', '').strip()
        except Exception as ex:
            crime_data['number_of_people_involved'] = ''
        print('number_of_people_involved -> ', crime_data['number_of_people_involved'])

        try:
            crime_data['cs_reference'] = html_soup.find('strong', string='CS reference:').parent.text \
                .replace('CS reference:', '').strip()
        except Exception as ex:
            crime_data['cs_reference'] = ''
        print('cs_reference -> ', crime_data['cs_reference'])

        try:
            crime_data['police_force'] = html_soup.find('strong', string='Police force:').parent.text \
                .replace('Police force:', '').strip()
        except Exception as ex:
            crime_data['police_force'] = ''
        print('police_force -> ', crime_data['police_force'])

        # Summary
        try:
            crime_data['summary'] = html_soup.find('h2', string='Summary').next_sibling.strip()
        except Exception as ex:
            crime_data['summary'] = ''
        print('summary -> ', crime_data['summary'])

        # Full Details section
        try:
            start_html_element = html_soup.find('h2', string='Full Details').next.next
            end_html_element = start_html_element.find_next('h2')
            full_details = get_raw_html(start_html_element, end_html_element)
            full_details = clean_text_from_tags(
                tags={'<br>': '', '<br/>': '', '<p>': '', '</p>': ''}, text=full_details
            )
            full_details = ' '.join(full_details.split())
            crime_data['full_details'] = full_details
        except Exception as ex:
            crime_data['full_details'] = ''
        print('full_details -> ', crime_data['full_details'])

        # Suspect description section
        try:
            whereabouts = html_soup.find('strong', string='Whereabouts:')
            crime_data['whereabouts'] = whereabouts.parent.text.replace('Whereabouts:', '').strip()
        except Exception as ex:
            crime_data['whereabouts'] = ''
        print('whereabouts -> ', crime_data['whereabouts'])

        try:
            sex = html_soup.find('strong', string='Sex:')
            crime_data['sex'] = sex.parent.text.replace('Sex:', '').strip()
        except Exception as ex:
            crime_data['sex'] = ''
        print('sex -> ', crime_data['sex'])

        try:
            age_range = html_soup.find('strong', string='Age range:')
            crime_data['age_range'] = age_range.parent.text.replace('Age range:', '').strip()
        except Exception as ex:
            crime_data['age_range'] = ''
        print('age_range -> ', crime_data['age_range'])

        try:
            height = html_soup.find('strong', string='Height:')
            crime_data['height'] = height.parent.text.replace('Height:', '').strip()
        except Exception as ex:
            crime_data['height'] = ''
        print('height -> ', crime_data['height'])

        try:
            build = html_soup.find('strong', string='Build:')
            crime_data['build'] = build.parent.text.replace('Build:', '').strip()
        except Exception as ex:
            crime_data['build'] = ''
        print('build -> ', crime_data['build'])

        try:
            hair_colour = html_soup.find('strong', string='Hair colour:')
            crime_data['hair_colour'] = hair_colour.parent.text.replace('Hair colour:', '').strip()
        except Exception as ex:
            crime_data['hair_colour'] = ''
        print('hair_colour -> ', crime_data['hair_colour'])

        try:
            hair_type = html_soup.find('strong', string='Hair type:')
            crime_data['hair_type'] = hair_type.parent.text.replace('Hair type:', '').strip()
        except Exception as ex:
            crime_data['hair_type'] = ''
        print('hair_type -> ', crime_data['hair_type'])

        try:
            facial_hair = html_soup.find('strong', string='Facial hair:')
            crime_data['facial_hair'] = facial_hair.parent.text.replace('Facial hair:', '').strip()
        except Exception as ex:
            crime_data['facial_hair'] = ''
        print('facial_hair -> ', crime_data['facial_hair'])

        try:
            ethnic_appearance = html_soup.find('strong', string='Ethnic appearance:')
            crime_data['ethnic_appearance'] = ethnic_appearance.parent.text.replace('Ethnic appearance:', '').strip()
        except Exception as ex:
            crime_data['ethnic_appearance'] = ''
        print('ethnic_appearance -> ', crime_data['ethnic_appearance'])

        # Additional Information section
        try:
            additional_information = html_soup.find('h2', string='Additional Information')
            crime_data['additional_information'] = additional_information.next_sibling.strip()
        except Exception as ex:
            crime_data['additional_information'] = ''
        print('additional_information -> ', crime_data['additional_information'])

        data.append(crime_data)

        # if needed loading page can be slow down
        # time.sleep(randint(100, 500)/100)

    return data


# save crime to db
def save_crime(crime_data):

    while crime_data:
        data = crime_data.pop()
        crime = CrimeModel(**data)
        crime.save()


class Scraper(ABC):
    def scrape_data(self) -> bool:
        pass

    def log_errors(self) -> None:
        pass


class CrimeScrapper(Scraper):
    def __init__(self, browser):
        self.browser = browser
        self.error = None
        self.exception = None
        self.status = True
        self.page_urls = []
        self.crime_urls = []
        self.crime_data = None

    def scrape_data(self) -> bool:
        self._get_pages_urls()
        self._get_crime_urls()
        self.get_crime_details()
        self._stop_browser()
        return self.status

    # get all pages with crimes
    def _get_pages_urls(self) -> None:

        try:
            page_urls = []
            most_wanted_url = config.CRIME_MOST_WANTED_URL
            self.browser.maximize_window()
            self.browser.get(most_wanted_url)
            self.browser.implicitly_wait(config.IMPLICITLY_WAIT_TIME)
            page_source = self.browser.page_source
            html_soup = BeautifulSoup(page_source, 'html.parser')

            paginator_links = html_soup.find_all('a', class_='page-link')
            for link in paginator_links:
                page_urls.append(link.get('href'))

            self.page_urls = list(set(page_urls))

        except Exception as ex:
            self.__handle_exception(msg='Error paginator urls can`t be extracted.', exception=ex)

    # get all urls for crime information
    def _get_crime_urls(self) -> None:

        crime_urls = []

        while self.page_urls:
            p_url = self.page_urls.pop()

            try:
                page_url = config.CRIME_BASE_URL + p_url
                self.browser.maximize_window()
                self.browser.get(page_url)
                self.browser.implicitly_wait(config.IMPLICITLY_WAIT_TIME)
                page_source = self.browser.page_source
                html_soup = BeautifulSoup(page_source, 'html.parser')

                figures = html_soup.find_all('figure', class_='d-flex align-items-center')

                for figure in figures:
                    a = figure.find('a')
                    crime_urls.append(a.get('href'))
            except Exception as ex:
                self.__handle_exception(msg='Crime url can`t be extracted.', exception=ex)
        self.crime_urls = crime_urls

    # get all crimes information
    def get_crime_details(self) -> None:

        data = []
        while self.crime_urls:

            crime_data = {}
            url = self.crime_urls.pop()
            crime_url = config.CRIME_BASE_URL + url

            try:
                self.browser.maximize_window()
                self.browser.get(crime_url)
                self.browser.implicitly_wait(config.IMPLICITLY_WAIT_TIME)
                page_source = self.browser.page_source
                html_soup = BeautifulSoup(page_source, 'html.parser')
            except Exception as ex:
                self.__handle_exception(msg='Crime page can`t be loaded.', exception=ex)

            # Crime url
            crime_data['crime_url'] = crime_url
            print('crime_url -> ', crime_data['crime_url'])

            # Wanted intro section
            try:
                crime_data['crime_pic_src'] = html_soup.find('figure').find('img').get('src')
            except Exception as ex:
                crime_data['crime_pic_src'] = ''
            print('crime_pic_src -> ', crime_data['crime_pic_src'])

            try:
                crime_data['crime_type'] = html_soup.find('strong', string='Crime type:').parent.text \
                    .replace('Crime type:', '').strip()
            except Exception as ex:
                crime_data['crime_type'] = ''
            print('crime_type -> ', crime_data['crime_type'])

            try:
                crime_data['crime_location'] = html_soup.find('strong', string='Crime location:').parent.text \
                    .replace('Crime location:', '').strip()
            except Exception as ex:
                crime_data['crime_location'] = ''
            print('crime_location -> ', crime_data['crime_location'])

            try:
                crime_data['suspect_name'] = html_soup.find('strong', string='Suspect name:').parent.text \
                    .replace('Suspect name:', '').strip()
            except Exception as ex:
                crime_data['suspect_name'] = ''
            print('suspect_name -> ', crime_data['suspect_name'])

            try:
                crime_data['nickname'] = html_soup.find('strong', string='Nickname:').parent.text \
                    .replace('Nickname:', '').strip()
            except Exception as ex:
                crime_data['nickname'] = ''
            print('nickname -> ', crime_data['nickname'])

            try:
                crime_data['number_of_people_involved'] = html_soup.find('strong',
                                                                         string='Number of people involved:').parent.text \
                    .replace('Number of people involved:', '').strip()
            except Exception as ex:
                crime_data['number_of_people_involved'] = ''
            print('number_of_people_involved -> ', crime_data['number_of_people_involved'])

            try:
                crime_data['cs_reference'] = html_soup.find('strong', string='CS reference:').parent.text \
                    .replace('CS reference:', '').strip()
            except Exception as ex:
                crime_data['cs_reference'] = ''
            print('cs_reference -> ', crime_data['cs_reference'])

            try:
                crime_data['police_force'] = html_soup.find('strong', string='Police force:').parent.text \
                    .replace('Police force:', '').strip()
            except Exception as ex:
                crime_data['police_force'] = ''
            print('police_force -> ', crime_data['police_force'])

            # Summary
            try:
                crime_data['summary'] = html_soup.find('h2', string='Summary').next_sibling.strip()
            except Exception as ex:
                crime_data['summary'] = ''
            print('summary -> ', crime_data['summary'])

            # Full Details section
            try:
                start_html_element = html_soup.find('h2', string='Full Details').next.next
                end_html_element = start_html_element.find_next('h2')
                full_details = get_raw_html(start_html_element, end_html_element)
                full_details = clean_text_from_tags(
                    tags={'<br>': '', '<br/>': '', '<p>': '', '</p>': ''}, text=full_details
                )
                full_details = ' '.join(full_details.split())
                crime_data['full_details'] = full_details
            except Exception as ex:
                crime_data['full_details'] = ''
            print('full_details -> ', crime_data['full_details'])

            # Suspect description section
            try:
                whereabouts = html_soup.find('strong', string='Whereabouts:')
                crime_data['whereabouts'] = whereabouts.parent.text.replace('Whereabouts:', '').strip()
            except Exception as ex:
                crime_data['whereabouts'] = ''
            print('whereabouts -> ', crime_data['whereabouts'])

            try:
                sex = html_soup.find('strong', string='Sex:')
                crime_data['sex'] = sex.parent.text.replace('Sex:', '').strip()
            except Exception as ex:
                crime_data['sex'] = ''
            print('sex -> ', crime_data['sex'])

            try:
                age_range = html_soup.find('strong', string='Age range:')
                crime_data['age_range'] = age_range.parent.text.replace('Age range:', '').strip()
            except Exception as ex:
                crime_data['age_range'] = ''
            print('age_range -> ', crime_data['age_range'])

            try:
                height = html_soup.find('strong', string='Height:')
                crime_data['height'] = height.parent.text.replace('Height:', '').strip()
            except Exception as ex:
                crime_data['height'] = ''
            print('height -> ', crime_data['height'])

            try:
                build = html_soup.find('strong', string='Build:')
                crime_data['build'] = build.parent.text.replace('Build:', '').strip()
            except Exception as ex:
                crime_data['build'] = ''
            print('build -> ', crime_data['build'])

            try:
                hair_colour = html_soup.find('strong', string='Hair colour:')
                crime_data['hair_colour'] = hair_colour.parent.text.replace('Hair colour:', '').strip()
            except Exception as ex:
                crime_data['hair_colour'] = ''
            print('hair_colour -> ', crime_data['hair_colour'])

            try:
                hair_type = html_soup.find('strong', string='Hair type:')
                crime_data['hair_type'] = hair_type.parent.text.replace('Hair type:', '').strip()
            except Exception as ex:
                crime_data['hair_type'] = ''
            print('hair_type -> ', crime_data['hair_type'])

            try:
                facial_hair = html_soup.find('strong', string='Facial hair:')
                crime_data['facial_hair'] = facial_hair.parent.text.replace('Facial hair:', '').strip()
            except Exception as ex:
                crime_data['facial_hair'] = ''
            print('facial_hair -> ', crime_data['facial_hair'])

            try:
                ethnic_appearance = html_soup.find('strong', string='Ethnic appearance:')
                crime_data['ethnic_appearance'] = ethnic_appearance.parent.text.replace('Ethnic appearance:',
                                                                                        '').strip()
            except Exception as ex:
                crime_data['ethnic_appearance'] = ''
            print('ethnic_appearance -> ', crime_data['ethnic_appearance'])

            # Additional Information section
            try:
                additional_information = html_soup.find('h2', string='Additional Information')
                crime_data['additional_information'] = additional_information.next_sibling.strip()
            except Exception as ex:
                crime_data['additional_information'] = ''
            print('additional_information -> ', crime_data['additional_information'])

            data.append(crime_data)

            # if needed loading page can be slow down
            # time.sleep(randint(100, 500)/100)
        self.crime_data = data

    # close browser
    def _stop_browser(self) -> None:
        self.browser.close()

    def __handle_exception(self, msg: str, exception: Exception):
        self.error = msg
        self.exception = exception
        self.status = False
        self.browser.close()

    def log_errors(self, logger: logging.Logger) -> None:
        logger.error(f'{self.error} - {self.exception}')



